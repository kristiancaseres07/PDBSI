const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');

// Initialization of env file
dotenv.config()


const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use("/users", userRoutes);


//  Database Connection
mongoose.connect(`mongodb+srv://${process.env.MONGODB_EMAIL}:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.uwt1xhh.mongodb.net/TestEncrypt?retryWrites=true&w=majority`, {
	useNewUrlParser:true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

		
app.listen(process.env.PORT , () => {
	console.log(`API is now online on port ${process.env.PORT}`);
})